import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  final String url = "https://skanaa.com/jastip/api/master_data";
  List dataCountry;
  var isLoading = false;

  void initState() {
    super.initState();
    this.getJsonData();
  }

  Future<String> getJsonData() async {
    if (this.mounted) {
      setState(() {
        isLoading = true;
      });
    }
    final response = await http
        .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});

    if (response.statusCode == 200) {
      if (this.mounted) {
        setState(() {
          var convertJson = json.decode(response.body);
          if(convertJson['status'] == 0){
            dataCountry = convertJson['data']['countries'];
            isLoading = false;
          }else{
            throw Exception(convertJson['message']);
          }
          
        });
      }
     
    } else {
      throw Exception('Failed to load post');
    }
  }

  void _snackBar(title, message) {
    Flushbar flush;
    flush = Flushbar(
      title: title,
      message: message,
      backgroundColor: Colors.green,
      duration: Duration(seconds: 3),
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      reverseAnimationCurve: Curves.decelerate,
      forwardAnimationCurve: Curves.elasticOut,
      isDismissible: false,
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      icon: Icon(
        Icons.check,
        color: Colors.greenAccent,
      ),
      mainButton: FlatButton(
        child: Text(
          "Close",
          style: TextStyle(color: Colors.amber),
        ),
        onPressed: () {
          flush.dismiss(true);
        },
      ),
    )..show(context);
    // docs link : https://pub.dev/packages/flushbar#-readme-tab-
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: dataCountry == null ? 0 : dataCountry.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Card(
                            child: Container(
                              color: Colors.red,
                              padding: EdgeInsets.all(20.0),
                              child: Row(
                                children: <Widget>[
                                  Text("Negara ${dataCountry[index]['country_name']} - ${dataCountry[index]['country_code']}" ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                },
              ));
  }
}
