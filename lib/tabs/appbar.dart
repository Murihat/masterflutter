import 'package:flutter/material.dart';

class MyAppBar extends AppBar {
  final Text title;
  final appbarIcons;
  final int index;  
  

  MyAppBar({this.title, this.appbarIcons, this.index})
    : super(
        title: title,
        actions: <Widget>[
          appbarIcons[index],  
        ]
      );
}
