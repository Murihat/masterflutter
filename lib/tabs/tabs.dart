import 'package:flutter/material.dart';
import 'package:masterflutter/drawer/drawer.dart';
import 'package:masterflutter/pages/home/home.dart';
import 'package:masterflutter/pages/account/account.dart';
import 'package:masterflutter/pages/about/about.dart';
import 'package:masterflutter/tabs/appbar.dart';
import 'package:badges/badges.dart';

class Tabs extends StatefulWidget {
  @override
  _TabsState createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  int _selectedIndex = 0;

  final _layoutPage = [Home(), About(), Account()];
  List<String> appbarTitles = ['Home', 'About', 'Account'];
  bool badge = false;

  void _onTabItem(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _badge() {
    setState(() {
      badge = (badge) ? false : true;
    });
  }

  void _showDialog(title, message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(message),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> appbarActions = [
      Row(
        children: <Widget>[
          Container(
            color: Colors.yellow,
            child: Badge(
              animationType: BadgeAnimationType.slide,
              position: BadgePosition.topRight(top: 0, right: 0),
              badgeColor: Colors.redAccent,
              badgeContent: Text('99',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0)),
              child: IconButton(
                icon: Icon(Icons.notifications, size: 22.0),
                onPressed: () {
                  _badge();
                },
              ),
            ),
          ),
          Container(
            color: Colors.green,
            child: Badge(
              animationType: BadgeAnimationType.slide,
              position: BadgePosition.topRight(top: 0, right: 0),
              badgeColor: Colors.redAccent,
              badgeContent: Text('99',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0)),
              child: IconButton(
                icon: Icon(Icons.message, size: 22.0),
                onPressed: () {
                  _showDialog("Yeay! Clicked",
                      "Thanks You, This is icon for message :)");
                },
              ),
            ),
          ),
        ],
      ),
      Row(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(5.0),
              color: Colors.deepOrangeAccent,
              child: Icon(Icons.notifications)),
          Container(
              padding: EdgeInsets.all(5.0),
              color: Colors.deepPurple,
              child: Icon(Icons.message)),
        ],
      ),
      Row(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(0),
              margin: EdgeInsets.all(0),
              // color: Colors.deepOrangeAccent,
              child: Badge(
                animationType: BadgeAnimationType.slide,
                position: BadgePosition.topRight(top: 0, right: 0),
                badgeColor: Colors.redAccent,
                badgeContent: Text('3'),
                child: Container(
                  child: IconButton(
                  icon: Icon(Icons.message, size: 22.0),
                  onPressed: () {
                    _showDialog("Yeay! Clicked",
                        "Thanks You, This is icon for message :)");
                  },)
                ),
              )),
          Container(
              padding: EdgeInsets.all(10.0),
              // color: Colors.deepPurple,
              child: Icon(Icons.message)),
        ],
      )
    ];

    return Scaffold(
      drawer: _selectedIndex == 0 ? DrawerSide() : null,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(40.0), // here the desired height
        child: MyAppBar(
          index: _selectedIndex,
          title: Text(appbarTitles[_selectedIndex]),
          appbarIcons: appbarActions,
        ),
      ),
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: SizedBox(
        height: 54.0,
        width: MediaQuery.of(context).size.width,
        child: BottomNavigationBar(
          backgroundColor: Colors.blueAccent,
          selectedItemColor: Colors.white,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home), title: Text('Home')),
            BottomNavigationBarItem(
                icon: Icon(Icons.help), title: Text('About')),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle), title: Text('Account')),
          ],
          type: BottomNavigationBarType.fixed,
          currentIndex: _selectedIndex,
          onTap: _onTabItem,
        ),
      ),
    );
  }
}
