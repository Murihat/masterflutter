import 'package:flutter/material.dart';
import 'dart:async';
import 'package:masterflutter/tabs/tabs.dart';


void main() => runApp(MaterialApp(debugShowCheckedModeBanner: false, home: SplashScreen()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //for remove debug banner
      title: 'Master Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new Tabs(),
    );
  }
}


class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(seconds: 5), onDoneLoading);
  }

  onDoneLoading() async {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => new Tabs()));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/exodus.gif'), 
          fit: BoxFit.cover
        ) ,
      ),
      // child: Center(
      //   child: CircularProgressIndicator(
      //     valueColor: AlwaysStoppedAnimation<Color>(Colors.redAccent),
      //   ),
      // ),
    );
  }
}